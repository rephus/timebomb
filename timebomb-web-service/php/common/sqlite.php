<?php 

/*
	getLastId(<Table>) => Return the last incremental id inserted
	clear(<Table>) => Remove content from table

	save<Table>(col1,col2,...) => Insert a single element in SQLite
	saveMultiple(Array(col1,col2) ) => Insert multiple rows 'in a row'
	get<Table>(id) => Get a single element by Id
	list<Table>() => Return all elements in the SQLite table
	search<Table>(col1,col2,...) => Search for elements in SQLite table

	listRandomTree(filterFolder, isFolder, limit) => Return x files or folders from the tree
	getRandomFolder(seed ) => Return a random folder (used for recommendation)
*/
include 'model.php';

if (!isset($pathDB)){
	$pathDB = "db/db.db";
	echo "opening TEST db : $pathDB \n";
}
$db = new PDO("sqlite:$pathDB"); 

function fixName($string){
	return str_replace("'","''",$string);
}

function getLastId($table){
	global $db;
	$sql = "SELECT id FROM $table ORDER BY id DESC LIMIT 1 ";
	$query = $db->query($sql);
	$row = $query->fetch();
	return $row['id'];
}

function saveScore($name,$timestamp,$level,$mode){
	global $db;
	$name = fixName($name);
	$sql = "INSERT INTO score (id, name, timestamp,level, mode) VALUES (NULL,'$name','$timestamp','$level','$mode')";
	$query =$db->prepare($sql);
	$stm = $query->execute();
	if (!$stm){
		//print_r($db->errorInfo());
		return 0;
	} else {
		return getLastId("score");	
	}
}

function listScore($mode,$limit){
	global $db;
	$sql = "SELECT * FROM score  WHERE mode = '$mode' ORDER BY level DESC LIMIT $limit";
	$i = 0;
	$array = Array();
	foreach ($db->query($sql) as $row) {
		$array[$i] = ModelScore($row);
		$i++;
	}
	//$json = array("woal"=>$array);
	//echo json_encode($json);
	return $array;
}
?> 
