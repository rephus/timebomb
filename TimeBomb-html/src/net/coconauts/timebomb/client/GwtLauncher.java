package net.coconauts.timebomb.client;

import net.coconauts.timebomb.client.ActionResolver;
import net.coconauts.timebomb.client.NativeFunctions;
import net.coconauts.timebomb.interfaces.Native;
import net.coconauts.timebomb.interfaces.ScoreResolver;
import net.coconauts.timebomb.system.Main;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class GwtLauncher extends GwtApplication {
	
	public static ScoreResolver resolver;
	public static Native nativeFunctions;
	@Override
	public GwtApplicationConfiguration getConfig () {
	
		GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(392, 653);
		return cfg;
	}

	@Override
	public ApplicationListener getApplicationListener () {
		resolver = new ActionResolver();
		nativeFunctions = new NativeFunctions();
		return new Main(resolver, nativeFunctions);
	}
}