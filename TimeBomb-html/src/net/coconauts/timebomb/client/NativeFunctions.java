package net.coconauts.timebomb.client;

import com.badlogic.gdx.Gdx;

import net.coconauts.timebomb.interfaces.Native;

public class NativeFunctions implements Native {

	@Override
	public void openURL(String url) {
		Gdx.app.debug("NativeFunctions","openURL - url: " + url);
	}

	@Override
	public void sendMail(String address) {
		Gdx.app.debug("NativeFunctions","sendMail - address: " + address);
	}
}
