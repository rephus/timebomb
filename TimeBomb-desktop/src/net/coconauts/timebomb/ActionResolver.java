package net.coconauts.timebomb;

import com.badlogic.gdx.Gdx;

import net.coconauts.timebomb.interfaces.ScoreResolver;

public class ActionResolver implements ScoreResolver{

	@Override
	public void log() {
		Gdx.app.debug("ActionResolver","log");

	}

	@Override
	public boolean isEnabled() {
		Gdx.app.debug("ActionResolver","isEnabled");
		return false;
	}

	@Override
	public void submitScore(String mode, long score) {
		Gdx.app.debug("ActionResolver","submitScore - mode: "+mode +", score: "+score);
	}

	@Override
	public void unlockAchievement(String achievement) {
		Gdx.app.debug("ActionResolver","unlockAchievement - achievement: "+achievement);
	}

	@Override
	public void incrAchievement(String achievement, int i) {
		Gdx.app.debug("ActionResolver","incrAchievement - achievement: "+achievement+", i "+i);
	}

	@Override
	public boolean isSigned() {
		Gdx.app.debug("ActionResolver","isSigned");
		return false;
	}

	@Override
	public void singedIn() {
		Gdx.app.debug("ActionResolver","singedIn");
	}

	@Override
	public void singOut() {
		Gdx.app.debug("ActionResolver","singOut");
	}

	@Override
	public void scoreBoardScreen() {
		Gdx.app.debug("ActionResolver","scoreBoardScreen");
	}

	@Override
	public void achievementScreen() {
		Gdx.app.debug("ActionResolver","achievementScreen");
	}
	
	public String getAchievementId(Achievements ach){
		Gdx.app.debug("ActionResolver","getAchievementId - achievement: "+ach);
		return null;
	}

	@Override
	public String getScoreboardArcade() {
		Gdx.app.debug("ActionResolver","getScoreboardArcade");
		return null;
	}

	@Override
	public String getScoreboardTimeAttack() {
		Gdx.app.debug("ActionResolver","getScoreboardTimeAttack");
		return null;
	}

	@Override
	public String getScoreboardUnsurvival() {
		Gdx.app.debug("ActionResolver","getScoreboardUnsurvival");
		return null;
	}
}
