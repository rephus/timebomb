package net.coconauts.timebomb;

import java.awt.Desktop;
import java.net.URI;
import com.badlogic.gdx.Gdx;
import net.coconauts.timebomb.interfaces.Native;

public class NativeFunctions implements Native {

	@Override
	public void openURL(String url) {
		Gdx.app.debug("NativeFunctions","openURL - url: " + url);
		try {
			Desktop.getDesktop().browse(new URI(url));
		} catch (Exception e) {
		}
		
	}
	@Override
	public void sendMail(String address) {
		Gdx.app.debug("NativeFunctions","sendMail - address: " + address);
		try {
			Desktop.getDesktop().browse(new URI("mailto:"+address));
		} catch (Exception e) {
		}
	}
	
}
