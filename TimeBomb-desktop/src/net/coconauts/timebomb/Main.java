package net.coconauts.timebomb;

import net.coconauts.timebomb.interfaces.Native;
import net.coconauts.timebomb.interfaces.ScoreResolver;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	
	public static ScoreResolver resolver;
	public static Native nativeFunctions;
	
	public static void main(String[] args)  {
		resolver = new ActionResolver();
		nativeFunctions = new NativeFunctions();
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Time Bomb (Ludum dare 27) by Javi Rengel";
		cfg.useGL20 = false;
		cfg.width = 392*3/2;
		cfg.height = 653*3/2;
		//cfg.width = 768;
		//cfg.height = 1280;
		new LwjglApplication
		(new net.coconauts.timebomb.system.Main(resolver,nativeFunctions), cfg);
	}
}
