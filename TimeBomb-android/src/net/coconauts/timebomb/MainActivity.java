package net.coconauts.timebomb;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import net.coconauts.timebomb.interfaces.Native;
import net.coconauts.timebomb.interfaces.ScoreResolver;
import net.coconauts.timebomb.system.Main;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends AndroidApplication implements ScoreResolver, Native{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = false;

		initialize(new Main(this,this), cfg);
	}

	@Override
	public void openURL(String url) {
		 Intent viewIntent = new Intent("android.intent.action.VIEW", 
		            Uri.parse(url));
		        startActivity(viewIntent);  
	}

	@Override
	public void sendMail(String address) {
		openURL("mailto:"+address);
	}

	@Override
	public void log() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void submitScore(String mode, long score) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unlockAchievement(String achievement) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void incrAchievement(String achievement, int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSigned() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void singedIn() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void singOut() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scoreBoardScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void achievementScreen() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAchievementId(Achievements id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getScoreboardArcade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getScoreboardTimeAttack() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getScoreboardUnsurvival() {
		// TODO Auto-generated method stub
		return null;
	}


}