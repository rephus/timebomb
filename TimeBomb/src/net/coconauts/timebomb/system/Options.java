package net.coconauts.timebomb.system;

public enum Options{ 
	//system
	sound(null), music(null), gui(null), rect(null), debug(null), language(null),width(null), height(null),
	// game
	dificulty(null) , level(null),
	// sprites
	sprite(null);
	
	String s;
	Options(String value){	this.s = value;	}
	Options(boolean value){this.s = String.valueOf(value);	}
	Options(int value){this.s = String.valueOf(value);	}
	
	public String getString(){	return s;}
	public boolean getBoolean(){return Boolean.valueOf(s);	}
	public int getInt(){return Integer.valueOf(s);	}
	
	public void set(String value){	this.s = value;	}
	public void set(boolean value){this.s = String.valueOf(value);	}
	public void set(int value){this.s = String.valueOf(value);	}
}
