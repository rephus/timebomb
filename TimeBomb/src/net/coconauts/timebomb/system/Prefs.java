package net.coconauts.timebomb.system;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.badlogic.gdx.Gdx;


public class Prefs {
	public final static String file = ".game-template";

	//Nexus one 392x653
	public static final int SCREEN_NORMAL_W = 480;
	public static final int SCREEN_NORMAL_H = 790;
	public static final int SCREEN_BIG_W = 640;
	public static final int SCREEN_BIG_H = 960;
	public static final int SCREEN_NEXUS4_W =  738;
	public static final int SCREEN_NEXUS4_H = 1280;
	
	
	public static final int SPRITE_BOMB = 0;
	public static final int SPRITE_PINNEAPLE = 1;
	public static final int MAX_SPRITES = 2;

	public static void reset(){
		//system
		Options.sound.set(true);
		Options.music.set(true);
		Options.gui.set(true);
		Options.rect.set(true); 
		Options.debug.set(false); 
		Options.language.set("es");
		Options.width.set(SCREEN_NORMAL_W);
		Options.height.set(SCREEN_NORMAL_H);
		// game
		Options.dificulty.set(1) ; 
		Options.level.set(1);
		//sprites
		Options.sprite.set(SPRITE_BOMB);
	}

	
	public static void load () {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(Gdx.files.external(file).read()));
			for (Options o : Options.values()){
				o.set(in.readLine());
			}
			
		} catch (Throwable e) {
			// :( It's ok we have defaults
		} finally {
			try {
				if (in != null) in.close();
			} catch (IOException e) {
			}
		}
	}

	public static void save () {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(Gdx.files.external(file).write(false)));
			for (Options o : Options.values()){
				out.write(o.getString());
			}
		
		} catch (Throwable e) {
		} finally {
			try {
				if (out != null) out.close();
			} catch (IOException e) {
			}
		}
	}

}
