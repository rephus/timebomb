/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.timebomb.system;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AssetsSounds {

    public static Sound explosion;
    public static Sound time;
    
    public static Music main;
    public static Music game;
    public static Music dead;
    
	public static void load () {
	  	Gdx.app.debug("AssetsSounds","Loading");   
	  	
        Gdx.app.debug("AssetsSounds","loading  sounds");
        explosion = Gdx.audio.newSound(Gdx.files.internal("sounds/explosion.wav"));
        time = Gdx.audio.newSound(Gdx.files.internal("sounds/time.wav"));

        main =  Gdx.audio.newMusic(Gdx.files.internal("sounds/main.ogg"));
        game =  Gdx.audio.newMusic(Gdx.files.internal("sounds/horse.ogg"));
        dead =  Gdx.audio.newMusic(Gdx.files.internal("sounds/dead.ogg"));

        Gdx.app.debug("AssetsSounds","end loading");
	}

	public static void playSound (Sound sound, float volume) {
		if (Options.sound.getBoolean()) sound.play(volume);
		
	}
	public static void playSound (Sound sound) {
		playSound(sound,1);
	}
	public static void stopMusic(){
		main.stop();
		game.stop();
		dead.stop();
	}
	public static void playMusic(Music music) {
		stopMusic();
		if (Options.music.getBoolean()) {
			music.setLooping(true);
			music.play();
		}
	}
}
