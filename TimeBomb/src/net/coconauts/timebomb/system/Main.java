package net.coconauts.timebomb.system;

import net.coconauts.timebomb.interfaces.Native;
import net.coconauts.timebomb.interfaces.ScoreResolver;
import net.coconauts.timebomb.objects.Enemy;
import net.coconauts.timebomb.screens.GameScreen;
import net.coconauts.timebomb.screens.MainScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class Main extends Game {
	boolean firstTimeCreate = true;
	public static ScoreResolver actionResolver ;
	public static Native nativeFunctions ;

	public long stopTime; 

	public Main (ScoreResolver resolver,Native nativeFunctions){
		Main.actionResolver = resolver;
		Main.nativeFunctions = nativeFunctions;
	}
	
	@Override
	public void create () {
		Prefs.reset();
		AssetsSprites.load();
		AssetsSounds.load();
		setScreen(new MainScreen(this));
	
	}
	
	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose () {
		super.dispose();
		Gdx.app.log(this.getClass().getName(), "dispose");
		getScreen().dispose();
	}

	@Override
	public void pause() {
		stopTime = System.currentTimeMillis();
		Gdx.app.log(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	
		long timeStopped =  System.currentTimeMillis() -stopTime;
		Gdx.app.log(this.getClass().getName(), "time stopped : "+timeStopped);	
		GameScreen.startTime +=  timeStopped; 
		
		if (GameScreen.level != null) for (Enemy b : GameScreen.level.enemies){
			b.initTime += timeStopped;
		}
		Gdx.app.log(this.getClass().getName(), "resume");	
	}


	@Override
	public void resize(int width, int height) {	Gdx.app.log(this.getClass().getName(), "resize");	}

}
