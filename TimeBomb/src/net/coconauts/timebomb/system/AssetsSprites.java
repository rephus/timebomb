/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.timebomb.system;

import java.util.HashMap;
import java.util.Map;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class AssetsSprites {

	public static BitmapFont font;
	public static BitmapFont dynFont;

    
    public static Sprite rabbit_stand;
    public static Sprite rabbit_move1;
    public static Sprite rabbit_move2;
    
    public static Sprite carrot;
    
    public static Sprite bomb;
    public static Sprite pineapple;

    public static Sprite explosion;
    public static Texture particle;
    
    
    public static Sprite background;
    public static Sprite hole;
    
    //icons
    public static Sprite soundOn;
    public static Sprite soundOff; 
    public static Texture back;
    public static Texture next;
    public static Texture prev;    
    
    //google play services 
    public static Map<String,Texture> leaderboards;
    public static Map<String,Texture> achievements;
    public static Map<String,Texture> play;

    //google play services 
    public static Texture facebook;
    public static Texture twitter;
    public static Texture email;
    public static Texture coconautsLogo;
    public static Texture gplus;
    public static Texture linkedin;

	public static Texture loadTexture (String file) {
		return new Texture(Gdx.files.internal(file));
	}

	public static void load () {
		int WIDTH = Options.width.getInt();
		int HEIGHT = Options.height.getInt();
		
	  	Gdx.app.debug("AssetsSprites","Loading ");   
	  	
	  	//fonts
        Gdx.app.debug("AssetsSprites","loading fonts");
		font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), Gdx.files.internal("fonts/font.png"), false);
		dynFont = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), Gdx.files.internal("fonts/font.png"), false);

        Gdx.app.debug("AssetsSprites","loading sprites ");
        bomb = new Sprite(loadTexture("sprites/bomb_medium.png"));
        pineapple = new Sprite(loadTexture("sprites/pineapple.png"));

        rabbit_stand = new Sprite(loadTexture("sprites/rabbit_stand.png"));
        rabbit_move1 = new Sprite(loadTexture("sprites/rabbit_move1.png"));
        rabbit_move2= new Sprite(loadTexture("sprites/rabbit_move2.png"));
        
        carrot = new Sprite(loadTexture("sprites/carrot.png"));
        
        background = new Sprite(loadTexture("sprites/background.png"));
		AssetsSprites.background.setSize(WIDTH,HEIGHT);

        hole = new Sprite(loadTexture("sprites/hole.png"));

        explosion = new Sprite(loadTexture("sprites/explosion_medium.png"));

        particle = loadTexture("sprites/particle.png");
        
        //icons
        Gdx.app.debug("AssetsSprites","loading icons");
        soundOn = new Sprite(loadTexture("icons/soundOn.png"));
        soundOff = new Sprite(loadTexture("icons/soundOff.png"));
        back = loadTexture("icons/back.png");
        next = loadTexture("icons/next.png");
        prev = loadTexture("icons/prev.png");
        
        //google play services 
        Gdx.app.debug("AssetsSprites","loading  google play services sprites");
        //if (Main.actionResolver.isEnabled()){
	        leaderboards = new HashMap<String,Texture>();
	        achievements= new HashMap<String,Texture>();
	        play= new HashMap<String,Texture>();
	        
	        leaderboards.put("gray", loadTexture("icons/gps/leaderboards_gray.png"));
	        leaderboards.put("green", loadTexture("icons/gps/leaderboards_green.png"));
	        leaderboards.put("white", loadTexture("icons/gps/leaderboards_white.png"));
	        achievements.put("gray", loadTexture("icons/gps/achievements_gray.png"));
	        achievements.put("green", loadTexture("icons/gps/achievements_green.png"));
	        achievements.put("white", loadTexture("icons/gps/achievements_white.png"));
	        play.put("gray", loadTexture("icons/gps/play_gray.png"));
	        play.put("green", loadTexture("icons/gps/play_green.png"));
	        play.put("white", loadTexture("icons/gps/play_white.png"));
        //}
        
        //about
        Gdx.app.debug("AssetsSprites","loading  about sprites");
        facebook = loadTexture("icons/about/facebook.png");
        twitter = loadTexture("icons/about/twitter.png");
        email = loadTexture("icons/about/email.png");
        coconautsLogo = loadTexture("icons/about/coconauts_logo.png");
        gplus = loadTexture("icons/about/gplus.png");
        linkedin  = loadTexture("icons/about/linkedin.png");

        
        //Settings.load(game.getFileIO());
        Gdx.app.debug("AssetsSprites","end loading");
	}
}
