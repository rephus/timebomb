package net.coconauts.timebomb.controllers;

import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.screens.MainScreen;
import net.coconauts.timebomb.screens.ScoreScreen;

public	class ScoreInput extends Input {
	
	ScoreScreen screen ;
	
	public ScoreInput(ScoreScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiBack.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new MainScreen(screen.game));
		}
		return false;
	}
}
