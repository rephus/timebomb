package net.coconauts.timebomb.controllers;

import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.screens.AboutScreen;
import net.coconauts.timebomb.screens.MainScreen;
import net.coconauts.timebomb.system.Main;

public	class AboutInput extends Input {
	
	AboutScreen screen ;
	
	public AboutInput(AboutScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiBack.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new MainScreen(screen.game));
		} else if (screen.uiLinkedinRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions
					.openURL("http://es.linkedin.com/pub/javier-rengel-jimenez/56/865/432/en");
		} else if (screen.uiEmailRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.sendMail("rephus@coconauts.net");
		} else if (screen.uiFacebookRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://www.facebook.com/rephus");
		} else if (screen.uiTwitterRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://twitter.com/rephus");
		} else if (screen.uiGPlusRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://plus.google.com/u/0/100118526666395537169/");
		} else if (screen.textLudumDare.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://www.ludumdare.com/compo/ludum-dare-27/?action=preview&uid=16605");
		} 

		return false;
	}
}
