package net.coconauts.timebomb.controllers;

import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.screens.MainScreen;
import net.coconauts.timebomb.screens.OptionScreen;
import net.coconauts.timebomb.system.AssetsSounds;
import net.coconauts.timebomb.system.Options;
import net.coconauts.timebomb.system.Prefs;

public	class OptionInput extends Input {
	
	OptionScreen screen ;
	
	public OptionInput(OptionScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiBack.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new MainScreen(screen.game));
		} else if (screen.uiSound.click(touchDownX, touchDownY) || screen.textSound.click(touchDownX, touchDownY)) {
			Options.sound.set(!Options.sound.getBoolean());
		} else if (screen.uiMusic.click(touchDownX, touchDownY) || screen.textMusic.click(touchDownX, touchDownY)) {
			Options.music.set(!Options.music.getBoolean());
			
			if (Options.music.getBoolean()) AssetsSounds.playMusic(AssetsSounds.main);
			else AssetsSounds.stopMusic();
		} else if (screen.uiBomb.click(touchDownX, touchDownY) || screen.textBomb.click(touchDownX, touchDownY)) {
			Options.sprite.set((Options.sprite.getInt()+1) % Prefs.MAX_SPRITES);
		} 
		return false;
	}
}
