package net.coconauts.timebomb.controllers;

import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.screens.AboutScreen;
import net.coconauts.timebomb.screens.GameScreen;
import net.coconauts.timebomb.screens.MainScreen;
import net.coconauts.timebomb.screens.OptionScreen;
import net.coconauts.timebomb.screens.ScoreScreen;
import net.coconauts.timebomb.system.Main;

import com.badlogic.gdx.Gdx;

public class MainInput extends Input {
	
	MainScreen screen ;
	
	public MainInput(MainScreen screen){
		this.screen = screen;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiGPSLogin.click(touchDownX, touchDownY)) {
			Main.actionResolver.singedIn();
		} else if (screen.uiGPSLogout.click(touchDownX, touchDownY)) {
			Main.actionResolver.singOut();
		} else if (screen.uiAchievements.click(touchDownX, touchDownY)) {
			Main.actionResolver.achievementScreen();
		} else if (screen.uiLeaderboards.click(touchDownX, touchDownY)) {
			Main.actionResolver.scoreBoardScreen();
		//} else if (screen.textGameTitle.click(touchDownX, touchDownY)) {
		} else if (screen.textPlay.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new GameScreen(screen.game,GameScreen.TEN_SECONDS));
		} else if (screen.textContinuous.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new GameScreen(screen.game,GameScreen.CONTINUOUS));
		}else if (screen.textOptions.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new OptionScreen(screen.game));
		} else if (screen.textAbout.click(touchDownX, touchDownY)) {
			Gdx.app.log("MainInput","About click ");
			screen.game.setScreen(new AboutScreen(screen.game));
		}else if (screen.textScoreboard.click(touchDownX, touchDownY)) {
			Gdx.app.log("MainInput","textScoreboard click ");
			screen.game.setScreen(new ScoreScreen(screen.game));
		}
		
		return false;
	}
}