package net.coconauts.timebomb.controllers;

import com.badlogic.gdx.Gdx;

import net.coconauts.timebomb.common.InputText;
import net.coconauts.timebomb.screens.GameScreen;
import net.coconauts.timebomb.screens.ScoreScreen;
import net.coconauts.timebomb.webservices.Request;
import net.coconauts.timebomb.webservices.ScoreBoard;

public class ScoreTextInput extends InputText{
	
		public long timestamp;
		public int level;
		public int mode;
		public ScoreTextInput(long timestamp, int level, int mode){
			this.timestamp = timestamp;
			this.level = level;
			this.mode = mode;
		}
	   @Override
	   public void input (String text) {
		   if (text.isEmpty() || text.equals("")){
			   ScoreTextInput listener = new ScoreTextInput(timestamp,level,mode);
				Gdx.input.getTextInput(listener, "Submit score (You must insert a name)", "Your name");
		   } else {

			   Gdx.app.log("ScoreInput", "saving score "+text+" "+timestamp+" "+level);
			   Request.sendRequest(ScoreBoard.placeScore(text, timestamp, level, mode));
			   GameScreen.game.setScreen(new ScoreScreen(GameScreen.game));
		   }
	   }

	   @Override
	   public void canceled () {
		   Gdx.app.log("ScoreInput", "canceled ");

	   }
}
