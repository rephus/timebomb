package net.coconauts.timebomb.controllers;

import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.screens.GameScreen;
import net.coconauts.timebomb.screens.MainScreen;

public class GameInput extends Input {
	
	GameScreen screen ;
	
	public GameInput(GameScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		
		x = (int) touchDownX;
		y = (int)touchDownY;
		
		if (screen.rBack.contains(x,y)){
			screen.game.setScreen(new MainScreen(screen.game));
		} else {
			screen.level.player.goTo(x,y);
		}
		
		return false;
	}

}
