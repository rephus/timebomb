
package net.coconauts.timebomb.objects;

import com.badlogic.gdx.math.MathUtils;

public class Particle extends GameObject {
	
	public static final int STATE_ALIVE = 0;	// particle is alive
	public static final int STATE_DEAD = 1;		// particle is dead
	
	// Time life in MS
	public static final int MAX_DIMENSION		= 5;	// the maximum width or height
	public static final int MAX_SPEED			= 10;	// maximum speed (per update)
	
	private int state;			// particle is alive or dead
	public float widht;		// width of the particle
	public float height;		// height of the particle
	private long init_time;		// initial time
	public int lifeTime ;
	
	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}
	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public Particle(float x, float y, int lifetime) {
		
		super(x,y);
		
		this.state = Particle.STATE_ALIVE;
		this.widht = MathUtils.random(1, MAX_DIMENSION);
		this.height = this.widht;
		this.init_time = System.currentTimeMillis();

		dirX = MathUtils.random(-MAX_SPEED,	MAX_SPEED);
		dirY =MathUtils.random(-MAX_SPEED,	MAX_SPEED);
		this.alpha = 1;
		this.lifeTime = lifetime;
	}
	

	public void update() {
		if (this.state != STATE_DEAD) {
			move(dirX, dirY);
			long actual_time = System.currentTimeMillis();
			// extract alpha

			int living_time = (int) ( actual_time - init_time);
			int remaining_time = living_time / lifeTime;
			alpha = lifeTime - remaining_time;
			
			if (actual_time - init_time >= lifeTime) {	// reached the end if its life
				this.state = STATE_DEAD;
			}
		}
	}

	@Override
	public String toString() {
		return "Particle [state=" + state + ", widht=" + widht + ", height="
				+ height + ", x=" + x + ", y=" + y + "]";
	}

}
