package net.coconauts.timebomb.objects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

import net.coconauts.timebomb.screens.GameScreen;
import net.coconauts.timebomb.screens.LevelScreen;

public abstract class GameObject {
	
	public List<Sprite> sprite;
	public float x;
	public float y;
	public float origX;
	public float origY;
	public boolean visible = true;
	public boolean rendered = true;
	public float w;
	public float h;
	public float rotation;
	public float dirX;
	public float dirY;
	public float alpha; //from 0 to 1

	public long initTime;
	
	public GameObject( float x,float y){
		sprite = new ArrayList<Sprite>();
		
		this.x = x; this.origX = x;
		this.y = y; this.origY = y;
		w = 0;	h = 0;
		rotation = 0;
		alpha = 1;
		initTime= System.currentTimeMillis();
	}
	
	public GameObject(Texture t,  float x,float y){
		this(new Sprite(t), x, y);
	}
	public GameObject(Texture t,  float x,float y, float w, float h){
		this(new Sprite(t), x, y, w, h);
	}
	
	public GameObject(Sprite s,  float x,float y){
		this(s, x,y , s.getWidth(), s.getHeight());
	}
	public GameObject(Sprite s,  float x,float y, float w, float h){
		this(x,y);
		s.setSize(w,h);
		sprite.add(s);
		setRectangle(w, h);
	}
	
	public void addSprite(Sprite s, float w, float h){
		s.setSize(w,h);
		sprite.add(s);
	}
	
	public void addSprite(Sprite s){
		addSprite(s,w,h);
	}
		
	public void setRectangle(float w, float h){
		this.w = w;
		this.h = h;
	}
	
	public Rectangle getRectangle(){
		return new Rectangle(x,y,w,h);
	}
	
	public void changeDirection(boolean x, boolean y){
		if (x) dirX = -dirX;
		if (y) dirY = -dirY;
	}
	public void move(float dx, float dy){
		changeDirection( (x > LevelScreen.WIDTH - w|| x < 0), (y > LevelScreen.HEIGHT -h || y < 0) );
		
		x += dirX;
		y += dirY;
	}
	
	public float distance(float x, float y) {
		//Distance formula d = sqrt( (x- x1)^2 + (y - y1)^2 )
		
		float varX = getCenterX() - x;
		float varY = getCenterY() - y;
		varX = varX * varX;
		varY = varY * varY;
		return (float) Math.sqrt(varX + varY);
	}
	public float distance(GameObject g) {
		return distance(g.getCenterX(),g.getCenterY());
	}

	public float getBottom(){
		return this.y+h;
	}
	public float getRight(){
		return this.x+w;
	}
	public float getCenterX(){
		return this.x+w/2;
	}
	public float getCenterY(){
		return this.y+h/2;
	}

	public boolean overlaps(GameObject s){
		return getRectangle().overlaps(s.getRectangle());
	}
	
	public boolean collide(GameObject s){
		boolean overlaps = this.overlaps(s);
		boolean isOverlaped = s.overlaps(this);
		return overlaps || isOverlaped;
	}
	
	public void draw(SpriteBatch batch ){
		
		if (rendered && visible){
			sprite.get(0).setPosition(x,y);
			sprite.get(0).draw(batch);
		}
	}
	
	public void draw(SpriteBatch batch, int i ){
		
		if (rendered && visible){
			sprite.get(i).setPosition(x,y);
			sprite.get(i).draw(batch);
		}
	}
	
	public boolean click(float x, float y ){
		return (getRectangle().contains(x, y));
	}
	public void update(){
		if (!rendered) return;
	}
	
	public void drawBorder(ShapeRenderer renderer) {
		renderer.begin(ShapeType.Rectangle);
		Rectangle r = getRectangle();
		renderer.rect(r.x,r.y,r.width,r.height);
		renderer.end();
	}
	public long lifeTimeMillis(){
		
		return  System.currentTimeMillis() -GameScreen.startTime- initTime;
	}
}
