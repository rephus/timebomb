package net.coconauts.timebomb.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Options;

public class Bomb extends Enemy{
	
	BitmapFont font ;
	public int radius = 100;

	public Sprite bomb ;
	public Sprite pineapple;
	
	public Bomb( float x, float y) {
		super(AssetsSprites.bomb,x,y,48,48);
		
		sprite.add(AssetsSprites.pineapple);
		
		this.font = AssetsSprites.font;

	}
	@Override 
	public void update(){
		super.update();
		long life = (System.currentTimeMillis() - initTime) / 1000 ;
		remaining = (int) (10 - life);
		
	}
	@Override
	public void draw(SpriteBatch batch){
		super.draw(batch,Options.sprite.getInt());
		
		/*switch (Options.sprite.getInt()){
			case Prefs.SPRITE_BOMB :font.setColor(Color.WHITE);	break;
			case Prefs.SPRITE_PINNEAPLE:font.setColor(Color.BLACK);	break;
		}*/
		font.setColor(Color.WHITE);	
		font.setScale(1);
		
		float  textX = x+w/3;
		if (remaining == 10){
			textX = x+5;
		}
		font.draw(batch,String.valueOf(remaining),textX,y+h*2/3);
		
	}

}
