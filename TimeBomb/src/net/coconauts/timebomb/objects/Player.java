package net.coconauts.timebomb.objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.coconauts.timebomb.system.AssetsSprites;


public class Player extends GameObject {

	public float destinationX;
	public float destinationY;
	
	public final static float SPEED_X = 1f;
	public final static float SPEED_Y = 1f;
	
	public Sprite carrot;
	
	public Player(Sprite t, float x, float y) {
		super(t, x, y);
		
		destinationX = x;
		destinationY = y;
		
		carrot = AssetsSprites.carrot;
		carrot.setSize(32,32);
		
	}
	@Override
	public void update(){
		super.update();
		if (isMoving())	move();
		
	}
	
	public boolean isMoving(){
		return (destinationX < getCenterX()-1 || destinationX > getCenterX()+1) || 
				(destinationY < getCenterY()-1 || destinationY > getCenterY()+1);
	}
	public void goTo(float dx , float dy ){
		destinationX = dx;
		destinationY = dy;
	}
	
	public void kill(){
		rendered = false;
	}
	public boolean isDead(){
		return !rendered;
	}
	
	public void draw(SpriteBatch batch ){
		if (!rendered || !visible)return;
		
		int i = 0; 
		
		batch.setColor(Color.WHITE);
		
		if (isMoving()){
			int angle = calcAngle(dirX, dirY) +180;

			long time = System.currentTimeMillis() % 500;
			i = (time  > 250)?1:2;
			batch.draw(sprite.get(i),x,y,
					w/2,h/2,w,h, 
					1f, 1f,
					angle, false);
			
			carrot.setPosition(destinationX-carrot.getWidth()/2,destinationY-carrot.getWidth()/2);
			carrot.draw(batch);
		} else{
			sprite.get(i).setPosition(x,y);
			sprite.get(i).draw(batch);
		}
		
	}
	public void move(){
		float difX = destinationX - getCenterX();
		float difY = destinationY - getCenterY();
		
		float factorX = Math.abs(difX /SPEED_X );
		float factorY = Math.abs(difY / SPEED_Y);
		float maxFactor = Math.max(factorX,factorY);
		
		dirX = difX / maxFactor;
		dirY = difY / maxFactor;
		//super.move(dirX, dirY);
		x += dirX;
		y += dirY;
	}
	public static int calcAngle(float x, float y) {

		double inRads = Math.atan2(y, x);
		int inDeg = (int) Math.toDegrees(inRads);

		if (inDeg >= 360)
			inDeg = inDeg - 360;
		if (inDeg < 0)
			inDeg = 360 + inDeg;

		return inDeg;
	}
}
