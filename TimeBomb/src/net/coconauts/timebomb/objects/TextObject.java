package net.coconauts.timebomb.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
public class TextObject  extends GameObject {
	
	public static final int ALIGN_LEFT = 0;
	public static final int ALIGN_CENTER = 1 ;
	public static final int ALIGN_RIGHT = 2;
	
	public String text;
	public int align;
	public int scale ;
	Color color;
	BitmapFont font ;
	
	public TextObject(String str, BitmapFont font , float x, float y) {
		this(str,font, x, y ,ALIGN_LEFT,1,Color.WHITE);
	}
	public TextObject(String str, BitmapFont font , float x, float y, Color color) {
		this(str,font, x, y ,ALIGN_LEFT,1,color);
	}
	
	public TextObject(String str,BitmapFont font ,float x, float y, int align, int scale, Color color) {
		super( x, y);
		
		this.text = str;
		this.font = font;
		
		this.align =align;
		this.scale =scale;
		this.color =color;
		
		setRectangle();
	}
	public void setRectangle(){
		font.setScale(this.scale);
		TextBounds bounds = font.getBounds(text.split("\n")[0]);
		super.setRectangle( bounds.width,bounds.height);
		
		this.x = alignTextX(x,w,align);
	}
	
	@Override
	public void draw(SpriteBatch batch ){
		font.setColor(color);
		font.setScale(scale);
		
		font.drawMultiLine(batch, text, x, y);
	}
	
	public void draw(SpriteBatch batch ,String text){
		this.text = text;
		draw(batch);
	}
		
	@Override
	public Rectangle getRectangle(){
		return new Rectangle(x,y-h,w,h);
	}

	public static int alignTextX(float x, float w, int align){
		 float alignment = 0;
		 switch (align){
			 case ALIGN_CENTER: alignment =w / 2 ; break;
			 case ALIGN_RIGHT: alignment = w; break;
		 }
		 return  (int) (x - alignment) ;
	}
	
	public static void  draw(SpriteBatch batch, String text, BitmapFont font ,float x, float y, int align, float scale, Color color){
		
		font.setColor(color);
		font.setScale(scale);
		
		TextBounds bounds = font.getBounds(text);
		x = alignTextX(x,bounds.width ,align);
		
		font.setColor(color);
		font.setScale(scale);
		font.drawMultiLine(batch, text, x, y);
	}
}