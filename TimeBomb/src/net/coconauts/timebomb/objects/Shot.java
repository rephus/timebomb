package net.coconauts.timebomb.objects;

public class Shot extends GameObject {

	private final int MAX_VELOCITY = 3;
	private final int DISSAPEAR_TIME = 1500;


	public Shot( float posX, float posY, float dirx, float diry) {
		
		super(  posX, posY);
		dirX = dirx*MAX_VELOCITY;
		dirY = diry*MAX_VELOCITY;
	}

	@Override
	public void update() {
		super.update();
		move(dirX,dirY);
	}
	
	public boolean end(){
		return lifeTimeMillis() >= DISSAPEAR_TIME;
	}
}
