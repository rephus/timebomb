package net.coconauts.timebomb.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import com.badlogic.gdx.Gdx;

public class Request{

	public static String sendRequest(String u) {
		try {
			//Gdx.app.log("Request", "Sending request "+u);
			final URL url = new URL(u);
			final URLConnection urlConnection = url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.connect();
	
			InputStream inputStream = urlConnection.getInputStream();

			 return readString(inputStream);
		} catch (Exception e) {
			Gdx.app.error("sendRequest", "Request "+u+" error "+e.getMessage());

			return null;
		}
	}
	
	public static String readString(InputStream is) throws IOException {
		  char[] buf = new char[2048];
		  Reader r = new InputStreamReader(is, "UTF-8");
		  StringBuilder s = new StringBuilder();
		  
		  while (true) {
		    int n = r.read(buf);
		    if (n < 0)
		      break;
		    s.append(buf, 0, n);
		  }
		  return s.toString();
		}
	
	
}