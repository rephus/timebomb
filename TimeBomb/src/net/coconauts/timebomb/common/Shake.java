package net.coconauts.timebomb.common;

import net.coconauts.timebomb.screens.LevelScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Shake implements Event {

	public static final int LOW = 1;
	public static final int MEDIUM = 5;
	public static final int HIGH = 10;

	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	// public int position;
	private long init_time;
	private int ms_alive;
	private int state; 
	private int intensity;
	private int last_move;
	private int maxShakes;
	private int nShakes;

	public Shake(int intensity, int ms, int maxShakes) {

		this.state = STATE_ALIVE;

		this.maxShakes = maxShakes;
		this.nShakes = 0;
		this.intensity = intensity;
		this.ms_alive = ms;
		this.last_move = LEFT;
		this.init_time = System.currentTimeMillis();
		
		Gdx.input.vibrate(ms);
	}

	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}

	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public void update() {

		OrthographicCamera cam = LevelScreen.cam;
		long actual_time = System.currentTimeMillis();
		if (actual_time - init_time >= ms_alive) {
			// execute new shake
			float x = cam.position.x;
			float y = cam.position.y;
			switch (this.last_move) {
			case LEFT:
				x += intensity;
				last_move = RIGHT;
				break;
			case RIGHT:
				x -= intensity;
				last_move = LEFT;
				break;

			}
			cam.position.set(x, y, 0);

			nShakes++;
			this.init_time = actual_time;
		}
		
		if (nShakes >= maxShakes){
			this.state = STATE_DEAD;
			cam.position.set(LevelScreen.WIDTH / 2, LevelScreen.HEIGHT / 2, 0);

		}
	}

	@Override
	public void draw(SpriteBatch batch) {

	}
}
