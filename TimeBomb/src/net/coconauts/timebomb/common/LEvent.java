package net.coconauts.timebomb.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LEvent {

	private List<Event> lEvent = new ArrayList<Event>();
	
	public void addEvent(Event e){
    	//public DynText( String str, int x, int y, int size, Color color, int seconds)
		lEvent.add(e);
    }
	
	public void drawEvents(SpriteBatch batch){
		for (Iterator<Event> iterador = lEvent.iterator(); iterador.hasNext();) {
			Event e = iterador.next();

			if (e.isAlive()) {
				e.draw(batch);
			} 
		}
	}
	public void updateEvents() {

		for (Iterator<Event> iterador = lEvent.iterator(); iterador.hasNext();) {
			Event e = iterador.next();

			if (e.isAlive()) {
				e.update();
			} else {
				iterador.remove();
			}
		}
	}
}
