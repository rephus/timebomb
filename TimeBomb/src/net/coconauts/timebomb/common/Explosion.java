package net.coconauts.timebomb.common;

import java.util.Arrays;

import net.coconauts.timebomb.objects.Particle;
import net.coconauts.timebomb.system.AssetsSprites;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Explosion implements Event {

	public static final int DEFAULT_PARTICLES = 10;
	public static final int DEFAULT_LIFETIME 	= 500;	
	public Particle[] particles; // particles in the explosion
	public int size; // number of particles
	public int state; // whether it's still active or not
	public float x, y;
	public Color color;
	
	public Sprite explosion_sprite;
	
	public Explosion(float x, float y) {
		this(DEFAULT_PARTICLES, x, y);
	}

	public Explosion(int particleNr, float x, float y) {
		this(particleNr,x,y, DEFAULT_LIFETIME, Color.YELLOW);
	}
	
	public Explosion(int particleNr, float x, float y,int lifetime, Color color) {
		this.state = STATE_ALIVE;
		this.x = x;
		this.y = y;
		this.color = color;
		
		this.particles = new Particle[particleNr];
		for (int i = 0; i < this.particles.length; i++) {
			Particle p = new Particle(x, y, lifetime);
			this.particles[i] = p;
		}
		this.size = particleNr;
	}
	public void setColor(Color c){
		this.color = c;
	}

	public void setSprite(Sprite t, int size){
		explosion_sprite = AssetsSprites.explosion;
		explosion_sprite.setSize(size*2, size *2);
	}
	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}

	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public void update() {
		if (this.state != STATE_DEAD) {
			boolean isDead = true;
			for (int i = 0; i < this.particles.length; i++) {
				if (this.particles[i].isAlive()) {
					this.particles[i].update();
					isDead = false;
				}
			}
			if (isDead)
				this.state = STATE_DEAD;
		}
	}

	@Override
	public String toString() {
		return "Explosion state=" + state + " [particles="
				+ Arrays.toString(particles) + ", size=" + size + "]";
	}

	@Override
	public void draw(SpriteBatch batch) {
	
		batch.setColor(color);
		if (explosion_sprite != null) {
			float w = explosion_sprite.getWidth();
		
			float h = explosion_sprite.getHeight();
			explosion_sprite.setPosition(x-w/2,y-h/2);
			explosion_sprite.draw(batch);
		}
		for (int i = 0; i < this.particles.length; i++) {
			Particle p = this.particles[i];
			if (p.isAlive()) {
				batch.draw(AssetsSprites.particle, (int) p.x, (int) p.y);
			}
		}
	}

}
