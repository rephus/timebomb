package net.coconauts.timebomb.common;

import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class Input implements InputProcessor {

	OrthographicCamera cam;
	Vector3 touchPoint;
	int WIDTH;
	int HEIGHT;
	
	public float touchDownX;
	public float touchDownY;
	public float touchUpX;
	public float touchUpY;
	public long touchDownTime;
	public long touchUpTime;
	public float touchDragX;
	public float touchDragY;

	public Input() {
		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);  
        cam.position.set(WIDTH / 2, HEIGHT / 2, 0);
		touchPoint = new Vector3();
		
		Gdx.input.setInputProcessor(this);
	}

	public void render() {
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		cam.update();
		cam.unproject(touchPoint.set(x, y, 0));
		x = (int) touchPoint.x;
		y = (int) touchPoint.y;

		touchDownX = x;
		touchDownY = y;
		touchDragX = x;
		touchDragY = y;
		touchDownTime = System.currentTimeMillis();

		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		cam.update();
		cam.unproject(touchPoint.set(x, y, 0));
		x = (int) touchPoint.x;
		y = (int) touchPoint.y;

		touchDragX = x;
		touchDragY = y;
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		cam.update();
		cam.unproject(touchPoint.set(x, y, 0));
		x = (int) touchPoint.x;
		y = (int) touchPoint.y;

		touchUpX = x;
		touchUpY = y;
		touchUpTime = System.currentTimeMillis();
		
		//touchDownTime = 0;

		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}
}
