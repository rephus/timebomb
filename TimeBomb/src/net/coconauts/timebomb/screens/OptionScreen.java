package net.coconauts.timebomb.screens;

import net.coconauts.timebomb.controllers.OptionInput;
import net.coconauts.timebomb.interfaces.ScoreResolver.Achievements;
import net.coconauts.timebomb.objects.GameObject;
import net.coconauts.timebomb.objects.TextObject;
import net.coconauts.timebomb.objects.UiObject;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Main;
import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;

public class OptionScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;

	public static int WIDTH;
	public static int HEIGHT;

	public OptionInput input; 
	
	//Ui Objects
	public GameObject uiBack ; // new UiObject(AssetsSprites.back, 10,10,64,64);

	public GameObject uiSound;
	public GameObject uiMusic;

	public TextObject textSound;
	public TextObject textMusic;

	public GameObject uiBomb;
	public TextObject textBomb;
	
	public OptionScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		this.level = new LevelScreen();
		this.input = new OptionInput(this);

		 initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		//Ui Objects
		uiBack = new UiObject(AssetsSprites.back, 10,10,64,64);

		uiSound = new UiObject(AssetsSprites.soundOn, 50, HEIGHT-120,48,48);
		uiSound.addSprite(AssetsSprites.soundOff);

		uiMusic = new UiObject(AssetsSprites.soundOn, 50, HEIGHT-120-50,48,48);
		uiMusic.addSprite(AssetsSprites.soundOff);

		uiBomb = new UiObject(AssetsSprites.bomb, 50, HEIGHT-120-100,48,48);
		uiBomb.addSprite(AssetsSprites.pineapple);
		
		textSound = new TextObject("Disable sound", AssetsSprites.font, 120, HEIGHT-85);
		textMusic = new TextObject("Disabe music", AssetsSprites.font, 120, HEIGHT-85-50);
		textBomb = new TextObject("Bomb", AssetsSprites.font, 120, HEIGHT-85-100);

	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	@Override
	public void render(float delta) {

		level.render(delta);
		update(delta);
		
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw(float deltaTime) {


		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawMenu();

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);

	}

	public void drawMenu() {
				
		uiBack.draw(batch);
		
		textSound.draw(batch,Options.sound.getBoolean()?"Disable sound":"Enable sound");
		uiSound.draw(batch,Options.sound.getBoolean()?0:1);
		
		textMusic.draw(batch,Options.music.getBoolean()?"Disable music":"Enable music");
		uiMusic.draw(batch,Options.music.getBoolean()?0:1);
		
		textBomb.draw(batch,Options.sprite.getInt()==0?"Bomb":"Explosive pineapple");
		uiBomb.draw(batch,Options.sprite.getInt());
		
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {
		if (level.isLevelEnded() )level.startLevel(10);

	}

	public void unlockMeetAchievement(){
		Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.MEET_THE_CREATOR));
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.log(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.log(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.log(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.log(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.log(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.log(this.getClass().getName(), "hide");	}
}
