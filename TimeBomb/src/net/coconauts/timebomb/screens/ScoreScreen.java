package net.coconauts.timebomb.screens;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import net.coconauts.timebomb.controllers.ScoreInput;
import net.coconauts.timebomb.interfaces.ScoreResolver.Achievements;
import net.coconauts.timebomb.objects.GameObject;
import net.coconauts.timebomb.objects.TextObject;
import net.coconauts.timebomb.objects.UiObject;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Main;
import net.coconauts.timebomb.system.Options;
import net.coconauts.timebomb.webservices.Request;
import net.coconauts.timebomb.webservices.Score;
import net.coconauts.timebomb.webservices.ScoreBoard;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.google.gson.Gson;

public class ScoreScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;

	public static int WIDTH;
	public static int HEIGHT;

	public ScoreInput input; 
	
	public ScoreBoard scoreBoard;
	//Ui Objects
	public GameObject uiBack ; // new UiObject(AssetsSprites.back, 10,10,64,64);

	
	public ScoreScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		this.level = new LevelScreen();
		this.input = new ScoreInput(this);

		 initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		//Ui Objects
		uiBack = new UiObject(AssetsSprites.back, 10,10,64,64);

		Gson gson = new Gson();
		String s = Request.sendRequest(ScoreBoard.listScore());
		try{
			scoreBoard = gson.fromJson(s, ScoreBoard.class);
		}catch (Exception e){
			Gdx.app.log("ScoreScreen", "Error in Gson conversion, s "+s+" error "+e.getMessage());
			
		}
		

		if (scoreBoard ==null) {
			Gdx.app.error("ScoreScreen", "Unable to get scores, response "+s);
			scoreBoard = new ScoreBoard();
			scoreBoard.score = new ArrayList<Score>();
		}
		// Gdx.app.log("nativefunctions openurl","Random "+f +" files "+f.files);
	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	@Override
	public void render(float delta) {

		level.render(delta);
		update(delta);
		
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw(float deltaTime) {


		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawScore();
			drawMenu();

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);

	}

	public void drawScore(){
		
		BitmapFont font = AssetsSprites.font;
		float column_left = 70;
		float column_middle = WIDTH/2;
		float column_right = WIDTH-50;
		float vertical_space = 50;
		int initY = HEIGHT - 10;

		Color color = Color.WHITE;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy\nHH:mm");
		int j = 1;
		
		if (scoreBoard.score.isEmpty()){
			TextObject.draw(batch, "Unable to get Scores" , font, column_middle, HEIGHT/2, TextObject.ALIGN_CENTER, 1,color);

		} else {
			TextObject.draw(batch, "10 seconds", font, column_left, initY, TextObject.ALIGN_LEFT, 1, color );
			TextObject.draw(batch, "Name", font, column_left, initY-vertical_space, TextObject.ALIGN_LEFT, 1, color );
			TextObject.draw(batch, "Date", font, column_middle, initY-vertical_space, TextObject.ALIGN_LEFT, 1, color);
			TextObject.draw(batch, "Level", font, column_right,initY- vertical_space, TextObject.ALIGN_RIGHT, 1, color);

			j = 1;
			for (Score s : scoreBoard.score){
		
				if (s.mode == 0){
					j++;
					color = (j==2)?Color.YELLOW:Color.WHITE;
					TextObject.draw(batch, String.valueOf(j-1) , font, 20, HEIGHT-(vertical_space*j), TextObject.ALIGN_LEFT, 1, color);
					TextObject.draw(batch, s.name.substring(0,Math.min(15, s.name.length())) , font, column_left, initY-(vertical_space*j), TextObject.ALIGN_LEFT, 0.5f, color);
					TextObject.draw(batch, dateFormat.format(new Date(s.timestamp)) , font, column_middle, initY-(vertical_space*j), TextObject.ALIGN_LEFT, 0.5f,color);
					TextObject.draw(batch, String.valueOf(s.level) , font, column_right, HEIGHT-(vertical_space*j), TextObject.ALIGN_RIGHT, 1,color);
				}
			}
			initY = HEIGHT/2;
			TextObject.draw(batch, "Continuous", font, column_left, initY, TextObject.ALIGN_LEFT, 1, color );
			TextObject.draw(batch, "Name", font, column_left, initY-vertical_space, TextObject.ALIGN_LEFT, 1, color );
			TextObject.draw(batch, "Date", font, column_middle, initY-vertical_space, TextObject.ALIGN_LEFT, 1, color);
			TextObject.draw(batch, "Time", font, column_right,initY- vertical_space, TextObject.ALIGN_RIGHT, 1, color);

			j = 1;
			for (Score s : scoreBoard.score){
				
				if (s.mode == 1){
					j++;
					color = (j==2)?Color.YELLOW:Color.WHITE;
					TextObject.draw(batch, String.valueOf(j-1) , font, 20, initY-(vertical_space*j), TextObject.ALIGN_LEFT, 1, color);
					TextObject.draw(batch, s.name.substring(0,Math.min(15, s.name.length())) , font, column_left, initY-(vertical_space*j), TextObject.ALIGN_LEFT, 0.5f, color);
					TextObject.draw(batch, dateFormat.format(new Date(s.timestamp)) , font, column_middle,initY-(vertical_space*j), TextObject.ALIGN_LEFT, 0.5f,color);
					TextObject.draw(batch, String.valueOf(s.level) , font, column_right, initY-(vertical_space*j), TextObject.ALIGN_RIGHT, 1,color);
				}
			}
		}
		
	}
	public void drawMenu() {
				
		uiBack.draw(batch);
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {
		if (level.isLevelEnded() )level.startLevel(10);

	}

	public void unlockMeetAchievement(){
		Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.MEET_THE_CREATOR));
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
