/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.timebomb.screens;

import net.coconauts.timebomb.controllers.GameInput;
import net.coconauts.timebomb.controllers.ScoreTextInput;
import net.coconauts.timebomb.objects.TextObject;
import net.coconauts.timebomb.system.AssetsSounds;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class GameScreen implements Screen {
	
	public static Game game;
	public static LevelScreen level;
	public OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer ;

	public static int WIDTH;
	public static int HEIGHT;
	
	public static int score = 0;
	public static long startTime; 
	public boolean gameEnded = false;
	public static int nLevel = 0;
	public long lifeTime ;
	
	//gamemodes
	public static final int TEN_SECONDS= 0;
	public static final int CONTINUOUS = 1;
	public int gameMode;
	public long lastBombCreated = 0;
	public long bombFrequency =5000;
	
	public GameInput input; 
	
	public Rectangle rBack = new Rectangle(10,10,64,64);
	
	public TextObject textLevel ;
	public TextObject textGameOver ;
	
	public GameScreen (Game game, int gameMode) {
		this.game = game;
		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);  
        cam.position.set(WIDTH / 2, HEIGHT / 2, 0);
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		level = new LevelScreen();
		input = new GameInput(this);
		this.gameMode = gameMode;
		startTime = System.currentTimeMillis();
		//Prefs.reset();
		level.player.rendered = true;
		
		AssetsSounds.playMusic(AssetsSounds.game);

		initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void initObjects(){
		textLevel = new TextObject("Level "+nLevel, AssetsSprites.font, 10 , HEIGHT-10);
		textGameOver = new TextObject("GAME OVER ", AssetsSprites.font, WIDTH/2, HEIGHT/2, TextObject.ALIGN_CENTER, 2, Color.RED );
	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	
	@Override
	public void render (float delta) {

		level.render(delta);

		update(delta);
		draw(delta);
		
	}

	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw (float deltaTime) {
		
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		batch.begin();
	 
		if (Options.gui.getBoolean()){
			drawGUI();
		}
		if (isGameEnded() ) drawEnd();
		
		batch.end();
		
		//SHAPE RENDERING
		renderer.setProjectionMatrix(cam.combined);		
	}

	public void drawGUI(){
		batch.draw(AssetsSprites.back, 10,10,64,64);
		
		if (gameMode == TEN_SECONDS  )textLevel.draw(batch);
		if (gameMode == CONTINUOUS ) {
			TextObject.draw(batch, "Time "+lifeTime+" s", AssetsSprites.font,10 , HEIGHT-10, TextObject.ALIGN_LEFT,1,Color.WHITE);
		
		}
		//draw score(score);
	}
	
	public void drawEnd(){
		textGameOver.draw(batch);
		//Utils.drawText(batch, AssetsSprites.font, "Game Over ", WIDTH/2, HEIGHT/2 , Utils.ALIGN_CENTER,2,new Color(1, 0, 0, 1));
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update (float deltaTime) {
		// Actual game logic
		if (gameMode == TEN_SECONDS && level.isLevelEnded() ) nextLevel();
		if (gameMode == CONTINUOUS ) nextContinuousLevel();

		if (isGameEnded() && !gameEnded) gameEnd();
	}
	

	public void gameEnd(){
		
		gameEnded = true;
		
		AssetsSounds.playMusic(AssetsSounds.dead);
		ScoreTextInput listener =  null;
		if (gameMode == TEN_SECONDS) listener = new ScoreTextInput(System.currentTimeMillis(),nLevel, TEN_SECONDS);
		if (gameMode == CONTINUOUS)listener = new ScoreTextInput(System.currentTimeMillis(),(int) lifeTime, CONTINUOUS);
		Gdx.input.getTextInput(listener, "Submit score", "Your name");			
			
	}
	
	public void nextContinuousLevel (){
		if (!isGameEnded() )lifeTime = (System.currentTimeMillis() - startTime) / 1000; 

		if (System.currentTimeMillis() - lastBombCreated > bombFrequency){
			lastBombCreated = System.currentTimeMillis();
			level.startLevel(1);
		}
		
		if (lifeTime < 10) bombFrequency = 5000;
		else if (lifeTime < 20) bombFrequency = 4000;
		else if (lifeTime < 30) bombFrequency = 3000;
		else if (lifeTime < 60) bombFrequency = 2000;
		else if (lifeTime < 90) bombFrequency = 1500;
		else if (lifeTime < 120 )bombFrequency = 1000;
		else if (lifeTime < 240 ) bombFrequency = 500;
	}
	public void nextLevel(){
		
		if (level.isLevelEnded() ){
			if (!isGameEnded()){
				nLevel++;
				textLevel.text = "Level "+nLevel;
			}
			level.startLevel(nLevel);
		}
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// GAME METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public  boolean isGameEnded(){
		return level.player.isDead();
	}
	
	
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.log(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.log(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.log(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.log(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.log(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.log(this.getClass().getName(), "hide");	}
}
