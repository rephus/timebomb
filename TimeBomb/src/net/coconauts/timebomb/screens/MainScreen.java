package net.coconauts.timebomb.screens;

import net.coconauts.timebomb.controllers.MainInput;
import net.coconauts.timebomb.objects.GameObject;
import net.coconauts.timebomb.objects.TextObject;
import net.coconauts.timebomb.objects.UiObject;
import net.coconauts.timebomb.system.AssetsSounds;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Main;
import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;

public class MainScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;
	public MainInput input ;
	
	public static int WIDTH;
	public static int HEIGHT;

	//Ui Objects
	public GameObject uiGPSLogin;
	public GameObject uiGPSLogout ;
	public GameObject uiAchievements ;
	public GameObject uiLeaderboards;
	//Text objects
	public GameObject textGameTitle ;
	public GameObject textPlay ;
	public GameObject textContinuous ;

	public GameObject textOptions;
	public GameObject textAbout ;
	public GameObject textScoreboard ;

	public MainScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		this.level = new LevelScreen();
		this.input = new MainInput(this);

		AssetsSounds.playMusic(AssetsSounds.main);
		initObjects();
	}
	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		
		
		uiGPSLogin = new UiObject(AssetsSprites.play.get("white"),200, 10,64,64);
		uiGPSLogout = new UiObject(AssetsSprites.play.get("green"),200, 10,64,64);
		uiAchievements = new UiObject(AssetsSprites.achievements.get("green"), 300, 10,64,64);
		uiLeaderboards = new UiObject(AssetsSprites.leaderboards.get("green"), 400, 10,64,64);
		//Text objects
		textGameTitle = new TextObject("Time bomb", AssetsSprites.font, WIDTH/2+5, HEIGHT - 20, TextObject.ALIGN_CENTER,3,Color.WHITE);
		textPlay = new TextObject("10 seconds\n      wave", AssetsSprites.font, WIDTH/2,  650, TextObject.ALIGN_CENTER,2,Color.WHITE);
		textContinuous = new TextObject("Continuous\n      mode", AssetsSprites.font, WIDTH/2,  500, TextObject.ALIGN_CENTER,2,Color.WHITE);

		textOptions = new TextObject( "Options", AssetsSprites.font, WIDTH/2,  250,TextObject.ALIGN_CENTER,2,Color.WHITE);
		textAbout = new TextObject( "About", AssetsSprites.font, WIDTH/2,  150, TextObject.ALIGN_CENTER,2,Color.WHITE);
		textScoreboard  = new TextObject( "Scoreboard", AssetsSprites.font, WIDTH/2,  350, TextObject.ALIGN_CENTER,2,Color.WHITE);
	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void render(float delta) {
		level.render(delta);
		update(delta);
		draw(delta);

		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void draw(float deltaTime) {
		//gl.glClearColor(0, 0, 0, 1);
		//gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawMenu();

		batch.end();

		// SHAPE RENDERING
		renderer.setProjectionMatrix(cam.combined);
		
	}

	public void drawMenu() {

		textGameTitle.draw(batch);
		textPlay.draw(batch);
		textContinuous.draw(batch);

		textAbout.draw(batch);
		textOptions.draw(batch);
		textScoreboard.draw(batch);
		
		if (Main.actionResolver.isEnabled()){
			if (Main.actionResolver.isSigned()){
				uiGPSLogout.draw(batch);
				uiAchievements.draw(batch);
				uiLeaderboards.draw(batch);
			} else {
				uiGPSLogin.draw(batch);
			}
		}
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {
		if (level.isLevelEnded() )level.startLevel(10);
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
