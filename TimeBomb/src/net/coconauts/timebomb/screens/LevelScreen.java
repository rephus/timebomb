package net.coconauts.timebomb.screens;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.coconauts.timebomb.common.Explosion;
import net.coconauts.timebomb.common.Input;
import net.coconauts.timebomb.common.LEvent;
import net.coconauts.timebomb.common.Shake;
import net.coconauts.timebomb.objects.Bomb;
import net.coconauts.timebomb.objects.Enemy;
import net.coconauts.timebomb.objects.GameObject;
import net.coconauts.timebomb.objects.Player;
import net.coconauts.timebomb.objects.UiObject;
import net.coconauts.timebomb.system.AssetsSounds;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

public class LevelScreen implements Screen {

	public static OrthographicCamera cam;
	public  SpriteBatch batch;
	public  final Matrix4 matrix = new Matrix4();	
	public  Vector3 touchPoint;
	public ShapeRenderer renderer ;
	
	public Input input; 
	public static int WIDTH;
	public static int HEIGHT;

	public Player player;

	public List<Enemy> enemies ;

	private int rotation = 0;
	private float ejex = 0;

	public LEvent lEvent = new LEvent();

	public long lastSecond = 0;
	
	public List<GameObject> holes;
	
	public LevelScreen() {
		initSystem();
		initLevel();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void initSystem() {

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);  
        cam.position.set(WIDTH / 2, HEIGHT / 2, 0);
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		batch = new SpriteBatch();
		batch.setColor(1,1,1,1);
		renderer = new ShapeRenderer();
		matrix.setToRotation(new Vector3(ejex, 0, 0), rotation);
		touchPoint = new Vector3();
		
		   GameScreen.nLevel =0;
		   
		holes= new ArrayList<GameObject>();
		input = new Input();
	}
	
	public void initLevel(){
		
		initPlayer();		
		initEnemies();
	}
	
	private void initPlayer(){
		player = new Player(AssetsSprites.rabbit_stand, WIDTH / 2, HEIGHT / 2);
		player.sprite.add(AssetsSprites.rabbit_move1);
		player.sprite.add(AssetsSprites.rabbit_move2);

		player.rendered = false;
	}
	
	private void initEnemies(){
		enemies = new ArrayList<Enemy>();
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
		
	@Override
	public void render (float delta) {
		update(delta);
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw (float deltaTime) {

		cam.update();
		
		//SPRITES RENDERING
		batch.setProjectionMatrix(cam.combined);
		
		batch.setTransformMatrix(matrix);
		batch.begin();
		
		AssetsSprites.background.draw(batch);
		for (GameObject h : holes){
			h.draw(batch);
		}
		lEvent.drawEvents(batch);
		player.draw(batch);

		for (Enemy e : enemies){
			e.draw(batch);
		}
		batch.end();
		
		//SHAPE RENDERING
		renderer.setProjectionMatrix(cam.combined);

		for  (Enemy e: enemies) {
			renderer.begin(ShapeType.Circle);
			if (GameScreen.nLevel < 5 && e.remaining >= 6 ) {renderer.setColor(Color.WHITE); renderer.circle(e.getCenterX(),e.getCenterY(), 100);}
			else if (GameScreen.nLevel < 10 && e.remaining < 6 && e.remaining >= 3 ){ renderer.setColor(Color.ORANGE); renderer.circle(e.getCenterX(),e.getCenterY(), 100);}
			else if (GameScreen.nLevel < 20 && e.remaining < 3){renderer.setColor(Color.RED); renderer.circle(e.getCenterX(),e.getCenterY(), 100);}
			
			renderer.end();
		}
	}

	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void update (float deltaTime) {

		lEvent.updateEvents();

		collision();
		
		player.update();
		
		updateEnemies();
		
		if (System.currentTimeMillis() / 1000 != lastSecond){
			lastSecond = System.currentTimeMillis() / 1000;
			AssetsSounds.playSound(AssetsSounds.time,0.5f);
		}

	}
	public void updateEnemies(){
		Iterator<Enemy> it = enemies.iterator();
		Enemy e;
		boolean destroy = false;
		while  (it.hasNext()) {
			e = (Enemy) it.next();
			e.update();
			
			if (e.remaining <= 0){
				it.remove();
				destroy = true;
				if (player.rendered && player.distance(e) < ((Bomb) e).radius){
					Gdx.app.log("updateEnemies","player killed by explosion "+player.distance(e.x, e.y));
					player.kill();
					lEvent.addEvent(new Explosion(500,player.getRight()-3,player.getBottom()-3, 1000,Color.RED));

				}
				explode(e.getCenterX(), e.getCenterY());
			} else if (e.remaining < 3){
				lEvent.addEvent(new Explosion(5,e.getRight()-3,e.getBottom()-3, 50,Color.YELLOW));
			}
		}
		if (destroy )AssetsSounds.playSound(AssetsSounds.explosion);
	}
	
	public void collision(){

		Iterator<Enemy> it = enemies.iterator();
		Enemy e;
		
		while  (it.hasNext()) {
			e = (Enemy) it.next();

			if (player.rendered && e.collide(player) ) {
				it.remove();
				Gdx.app.log("updateEnemies","player killed by collision ");
				player.kill();
				lEvent.addEvent(new Explosion(500,player.getRight()-3,player.getBottom()-3, 500,Color.RED));

				explode(e.getCenterX(), e.getCenterY());
				AssetsSounds.playSound(AssetsSounds.explosion);
			} 
			/*
			for (Enemy e2: enemies){
				
				if (e != e2 && e.collide(e2) ){
					e.changeDirection(true,true);
					e2.changeDirection(true,true);
				}
			}*/
		}
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// LEVEL METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void explode(float x, float y) {
		Explosion ex = new Explosion( x, y);
		ex.setSprite(AssetsSprites.explosion,100);
		ex.setColor(Color.YELLOW);
		lEvent.addEvent(ex);
		lEvent.addEvent(new Shake(5,  50, 5));
		
		Sprite h = AssetsSprites.hole;
		holes.add(new UiObject(h,x-h.getWidth()/2, y-h.getHeight()/2,100,100));
		if (holes.size() > 50){
			holes.remove(0);
		}
	}
	
	public void startLevel(int n){
		for (int i= 0;i< n ; i++){
			float [] xy = getRandomEdgePoint();

			Bomb b= new Bomb(xy[0],xy[1]);
			enemies.add(b);
		}
	}
	
	public boolean isLevelEnded(){
		return enemies.isEmpty();
	}

	
	public float[] getRandomEdgePoint(){
		float x=0 ,y=0 ;
		
		int border = MathUtils.random(0,3);

		switch (border){
		case 0: x=0;
				y=MathUtils.random(0,HEIGHT);
				break;
		case 1: x=MathUtils.random(0,WIDTH);
				y=HEIGHT-48;
				break;
		case 2: x=WIDTH-48;
				y=MathUtils.random(0,HEIGHT);
				break;
		case 3: x=MathUtils.random(0,WIDTH);
				y=0;
				break;
		}
		
		float[] xy = new float[2];
		xy[0] =x;
		xy[1] = y;
		return xy;
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.log(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.log(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.log(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.log(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.log(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.log(this.getClass().getName(), "hide");	}

}
