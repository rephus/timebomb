package net.coconauts.timebomb.screens;

import net.coconauts.timebomb.controllers.AboutInput;
import net.coconauts.timebomb.interfaces.ScoreResolver.Achievements;
import net.coconauts.timebomb.objects.GameObject;
import net.coconauts.timebomb.objects.TextObject;
import net.coconauts.timebomb.objects.UiObject;
import net.coconauts.timebomb.system.AssetsSprites;
import net.coconauts.timebomb.system.Main;
import net.coconauts.timebomb.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;

public class AboutScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;

	public static int WIDTH;
	public static int HEIGHT;

	public AboutInput input; 
	
	//Ui Objects
	public GameObject uiBack ; // new UiObject(AssetsSprites.back, 10,10,64,64);

	public GameObject uiLinkedinRephus ; // new UiObject(AssetsSprites.linkedin, 260, HEIGHT-520,32,32);
	public GameObject uiEmailRephus ; // new UiObject(AssetsSprites.email, 300, HEIGHT-520,32,32);
	public GameObject uiFacebookRephus ; // new UiObject(AssetsSprites.facebook, 340, HEIGHT-520,32,32);
	public GameObject uiTwitterRephus ; // new UiObject(AssetsSprites.twitter, 380, HEIGHT-520,32,32);
	
	public GameObject uiGPlusRephus ; // new UiObject(AssetsSprites.gplus, 420, HEIGHT-520,32,32);
	
	//Text objects
	public GameObject textGameDeveloped ; // new TextObject("A game developed by", AssetsSprites.font, WIDTH/2, HEIGHT-450,  TextObject.ALIGN_CENTER,1,Color.WHITE);
	public GameObject textJavier ; // new TextObject("Javier Rengel", AssetsSprites.font,10, HEIGHT-500);
	public GameObject textLudumDare;
	
	public AboutScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		//this.level = new LevelScreen();
		this.input = new AboutInput(this);

		 initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		//Ui Objects
		uiBack = new UiObject(AssetsSprites.back, 10,10,64,64);

		uiLinkedinRephus = new UiObject(AssetsSprites.linkedin, 260, HEIGHT-120,32,32);
		uiEmailRephus = new UiObject(AssetsSprites.email, 300, HEIGHT-120,32,32);
		uiFacebookRephus = new UiObject(AssetsSprites.facebook, 340, HEIGHT-120,32,32);
		uiTwitterRephus = new UiObject(AssetsSprites.twitter, 380, HEIGHT-120,32,32);
		uiGPlusRephus = new UiObject(AssetsSprites.gplus, 420, HEIGHT-120,32,32);

		//Text objects
		textGameDeveloped = new TextObject("A game developed by", AssetsSprites.font, WIDTH/2, HEIGHT-50,  TextObject.ALIGN_CENTER,1,Color.WHITE);
		textJavier = new TextObject("Javier Rengel", AssetsSprites.font,10, HEIGHT-100, TextObject.ALIGN_LEFT,1,Color.WHITE);
		textLudumDare = new TextObject("For LudumDare 27", AssetsSprites.font, WIDTH/2, HEIGHT-150,  TextObject.ALIGN_CENTER,1,Color.BLUE);

	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	@Override
	public void render(float delta) {

		update(delta);
		
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		//level.render(delta);
		
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw(float deltaTime) {


		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		AssetsSprites.background.draw(batch);

			drawMenu();

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);

	}

	public void drawMenu() {
		
		textGameDeveloped.draw(batch);
		textJavier.draw(batch);
		textLudumDare.draw(batch);
		
		uiBack.draw(batch);
		
		uiLinkedinRephus.draw(batch);
		uiEmailRephus.draw(batch);
		uiFacebookRephus.draw(batch);
		uiTwitterRephus.draw(batch);
		uiGPlusRephus.draw(batch);
		
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {

	}

	public void unlockMeetAchievement(){
		Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.MEET_THE_CREATOR));
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.log(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.log(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.log(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.log(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.log(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.log(this.getClass().getName(), "hide");	}
}
